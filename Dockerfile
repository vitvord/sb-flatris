FROM node:12
ARG APPDIR=/opt/app
RUN mkdir $APPDIR
WORKDIR $APPDIR
COPY package.json yarn.lock ./
RUN yarn --frozen-lockfile
COPY . ./
RUN yarn build
RUN yarn test
CMD yarn start
EXPOSE 3000
